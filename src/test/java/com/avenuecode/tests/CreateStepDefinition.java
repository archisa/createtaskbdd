package com.avenuecode.tests;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

import com.avenuecode.pages.CreateTaskPage;
import com.avenuecode.pages.LandingPage;
import com.avenuecode.pages.LoginPage;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class CreateStepDefinition extends Base {

	@Given("^User logs in$")
	public void navigate() {
		intializeDriver();
		dr.get("http://qa-test.avenuecode.com");
	}

	@When("^User is logged in using username as \"(.*)\" and password as \"(.*)\"$")
	public void login(String username, String password) {
		lp = new LandingPage(dr);
		lp.myTasks.click();
		login = new LoginPage(dr);
		login.Login(username, password);
		dr.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		task = new CreateTaskPage(dr);
	}

	@Then("^The message \"(.*)\" should be displayed$")
	public void verifySuccessful(String message) {
		String expectedText = message;
		String actualText = task.messageLabel.getText();
		Assert.assertTrue("Wrong message displayed", expectedText.equals(actualText));

	}

	@Then("^Verify max length for text field$")
	public void verify_max_length_for_text_field() throws Throwable {
		String max = task.newTaskField.getAttribute("maxlength");
		Assert.assertEquals("Max length exceeds 250 characters", 250, Integer.parseInt(max));

	}

	@When("User enters value as \"(.*)\" in task field and presses enter")
	public void createTask(String value) {
		taskRows = dr.findElements(By.xpath(task.taskTable));
		task.newTaskField.sendKeys(value);
		task.newTaskField.sendKeys(Keys.ENTER);
	}

	@When("^User clicks on Add Task button$")
	public void user_clicks_on_Add_Task_button() throws Throwable {
		task.newTaskButton.click();
	}

	@Then("^Edit Empty Field To add description as \"([^\"]*)\"$")
	public void edit_Empty_Field_To_add_description_as(String desc) throws Throwable {
		taskRows = dr.findElements(By.xpath(task.taskTable));
		dr.findElement(By.linkText("empty")).click();
		dr.findElement(By.xpath(task.inputTaskName)).sendKeys(desc);
	}

	@Then("^A new task should be created$")
	public void verifyTaskCreated() {
		List<WebElement> newrows = dr.findElements(By.xpath(task.taskTable));
		Assert.assertEquals("Task was not added", taskRows.size() + 1, newrows.size());

	}

	@Then("^User edits the newly created task with \"([^\"]*)\"$")
	public void user_edits_the_newly_created_task_with(String value) throws Throwable {
		findElement(task.editTaskDesc).click();
		findElement(task.taskDescInput).clear();
		findElement(task.taskDescInput).sendKeys(value);
		Thread.sleep(10000);
	}

	@Then("^Task description value should be unchanged \"(.*)\"$")
	public void task_description_should_not_be_edited(String value) throws Throwable {
		Assert.assertEquals("Task Description edited", value, findElement(task.editTaskDesc).getText());
		Thread.sleep(1000);

	}

	@When("^User enters value as \"([^\"]*)\" two characters in task field and presses enter$")
	public void user_enters_value_as_two_characters_in_task_field_and_presses_enter(String desc) throws Throwable {
		taskRows = dr.findElements(By.xpath(task.taskTable));
		task.newTaskField.sendKeys(desc);
		task.newTaskField.sendKeys(Keys.ENTER);

	}

	@Then("^clicks on Tick Button$")
	public void clicks_on_Tick_Button() throws Throwable {
		dr.findElement(By.xpath(task.tickButton)).click();
	}

	@Then("^clicks on Cross Button$")
	public void clicks_on_Cross_Button() throws Throwable {
		JavascriptExecutor myExecutor = (JavascriptExecutor) dr;
		myExecutor.executeScript("arguments[0].click();", dr.findElement(By.xpath(task.crossButton)));
		Thread.sleep(10000);
	}

	@Then("^A new task should not be created$")
	public void a_new_task_should_not_be_created() throws Throwable {
		List<WebElement> newrows = dr.findElements(By.xpath(task.taskTable));
		Assert.assertEquals("Negative Scenario: Task should not be added", taskRows.size(), newrows.size());
	}

	@Then("^I close the browser$")
	public void closeBrowser() {
		dr.close();
		dr.quit();
	}
}
