package com.avenuecode.tests;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import com.avenuecode.pages.CreateSubTaskPage;
import com.avenuecode.pages.CreateTaskPage;
import com.avenuecode.pages.LandingPage;
import com.avenuecode.pages.LoginPage;

public class Base {
	
	public static WebDriver dr;
	public CreateTaskPage task;
	public LandingPage lp;
	public LoginPage login;
	public List<WebElement> taskRows;
	public List<WebElement> subTaskRows;
	public CreateSubTaskPage subTask;
	

	public void intializeDriver() {
		 System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+"/chromedriver");
		   dr=new ChromeDriver();
	}
	
	public WebElement findElement(String element) {
		return dr.findElement(By.xpath(element));
		
	}
}
