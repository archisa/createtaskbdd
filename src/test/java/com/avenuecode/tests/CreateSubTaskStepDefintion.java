package com.avenuecode.tests;

import java.util.List;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.avenuecode.pages.CreateSubTaskPage;
import com.avenuecode.pages.CreateTaskPage;

import cucumber.api.java.en.Then;

public class CreateSubTaskStepDefintion extends Base {

	@Then("^User should be able to see Manage Subtasks button$")
	public void user_should_be_able_to_see_Manage_Subtasks_button() throws Throwable {
		task = new CreateTaskPage(dr);
		Assert.assertTrue("Button not visible", findElement(task.manageSubTask).isDisplayed());
	}

	@Then("^User clicks on Manage Subtasks button$")
	public void user_clicks_on_Manage_Subtasks_button() throws Throwable {
		task = new CreateTaskPage(dr);
		findElement(task.manageSubTask).click();
		subTask = new CreateSubTaskPage(dr);
	}

	@Then("^User enters value as \"([^\"]*)\" and date as \"([^\"]*)\"$")
	public void user_clicks_on_Manage_Subtasks_button(String value, String date) throws Throwable {
		subTaskRows = dr.findElements(By.xpath(subTask.subTaskTable));
		subTask.newSubTaskField.sendKeys(value);
		subTask.dueDate.sendKeys(date);
		subTask.addSubTask.click();
	}

	@Then("^A new sub task should be created$")
	public void a_new_sub_task_should_be_created() throws Throwable {
		List<WebElement> newrows = dr.findElements(By.xpath(subTask.subTaskTable));
		Assert.assertEquals("Task was not added", subTaskRows.size() + 1, newrows.size());
	}

	@Then("^Number of subtasks are displayed on button$")
	public void a_new_sub_task_is_created() throws Throwable {
		subTask.close.click();
		Assert.assertEquals("Manage subtasks button is not showing the newly added subtask", "(1) Manage Subtasks",
				findElement(task.manageSubTask).getText());
	}

	@Then("^User is able to edit the subtask desc with value as \"(.*)\"$")
	public void i_am_able_to_edit_the_subtask(String value) throws Throwable {
		findElement(subTask.subTaskDesc).click();
		findElement(subTask.subTaskDescInput).clear();
		findElement(subTask.subTaskDescInput).sendKeys(value);

	}

	@Then("^User is able to save changes with value as \"(.*)\" when he clicks on tick button$")
	public void i_am_able_to_save_changes_when_I_click_on_tick_button(String value) throws Throwable {
		findElement(subTask.tickButton).click();
		Assert.assertEquals("SubTask Description was not edited", value, findElement(subTask.subTaskDesc));
		Thread.sleep(10000);
	}

	@Then("^User is able to cancel changes when he clicks on cancel button and value \"(.*)\" is not saved$")
	public void i_am_able_to_cancel_changes_when_I_click_on_cancel_button(String value) throws Throwable {
		findElement(subTask.cancelButton).click();
		Assert.assertNotEquals("Negative Scenario: SubTask Description should not be edited", value,
				findElement(subTask.subTaskDesc));
		Thread.sleep(10000);
	}

	@Then("^TaskID and Description should be read only fields on Manage Subtasks modal dialog$")
	public void taskid_and_Description_should_be_read_only_fields_on_Manage_Subtasks_modal_dialog() throws Throwable {
		Assert.assertFalse("Task description is enabled", subTask.taskDesc.isEnabled());
	}

	@Then("^SubTask Description should not accept more than (\\d+) characters$")
	public void subtask_Description_should_not_accept_more_than_characters(int arg1) throws Throwable {
		String max = subTask.taskDesc.getAttribute("maxlength");
		Assert.assertEquals("Max length exceeds 250 characters", 250, Integer.parseInt(max));
	}

	@Then("^User enters value as \"([^\"]*)\"$")
	public void user_enters_value_as(String value) throws Throwable {
		subTaskRows = dr.findElements(By.xpath(subTask.subTaskTable));
		subTask.newSubTaskField.sendKeys(value);
		subTask.addSubTask.click();
	}

	@Then("^User enters date as \"([^\"]*)\"$")
	public void user_enters_date_as(String date) throws Throwable {
		subTaskRows = dr.findElements(By.xpath(subTask.subTaskTable));
		subTask.dueDate.sendKeys(date);
		subTask.addSubTask.click();
	}

	@Then("^A new sub task should not be created$")
	public void a_new_sub_task_should_not_be_created() throws Throwable {
		List<WebElement> newrows = dr.findElements(By.xpath(subTask.subTaskTable));
		Assert.assertEquals("Negative Scenario: SubTask should not be added", subTaskRows.size(), newrows.size());
		subTask.close.click();
		Assert.assertEquals("Negative Scenario: Manage subtasks button should not show newly added subtask ",
				"(0) Manage Subtasks", findElement(task.manageSubTask).getText());
	}

}
