package com.avenuecode.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class CreateSubTaskPage {

	public CreateSubTaskPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

	@FindBy(how = How.XPATH, using = "//*[@name='new_sub_task']")
	public WebElement newSubTaskField;

	@FindBy(how = How.XPATH, using = "//*[@id='add-subtask']")
	public WebElement addSubTask;

	@FindBy(how = How.XPATH, using = "//*[@ng-click='close()']")
	public WebElement close;

	@FindBy(how = How.XPATH, using = "//*[@id='dueDate']")
	public WebElement dueDate;

	@FindBy(how = How.XPATH, using = "//*[@id='edit_task']")
	public WebElement taskDesc;

	@FindBy(how = How.XPATH, using = "//h3")
	public WebElement taskID;

	public String subTaskTable = "//div[@ng-show='task.sub_tasks.length']//table[@class='table']/tbody/tr";

	public String subTaskDesc = "//div[@ng-show='task.sub_tasks.length']//table[@class='table']/tbody/tr[1]//*[@class='ng-scope ng-binding editable editable-click']";

	public String subTaskDescInput = "//*[@ng-model='$data']";

	public String tickButton = "//*[@class='editable-buttons']//*[@class='btn btn-primary']";

	public String cancelButton = "//*[@ng-click='$form.$cancel()']";
}
