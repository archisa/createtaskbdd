package com.avenuecode.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class CreateTaskPage {

	public CreateTaskPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}

	@FindBy(how = How.XPATH, using = "//*[@id='new_task']")
	public WebElement newTaskField;

	@FindBy(how = How.XPATH, using = "//div//h1")
	public WebElement messageLabel;

	public String taskTable = "//table[@class='table']/tbody/tr";

	@FindBy(how = How.XPATH, using = "//*[@class='input-group-addon glyphicon glyphicon-plus']")
	public WebElement newTaskButton;

	public String emptyTask = "//*[@class='ng-scope ng-binding editable editable-click editable-empty xh-highlight']";

	public String inputTaskName = "//*[@ng-model='$data']";

	public String tickButton = "//*[@class='editable-buttons']//*[@class='btn btn-primary']";

	public String crossButton = "//*[@ng-click='$form.$cancel()']";

	public String manageSubTask = "//tr[1]//*[@ng-click='editModal(task)']";

	public String editTaskDesc = "//table//tr[1]//*[@class='ng-scope ng-binding editable editable-click']";

	public String taskDescInput = "//*[@ng-model='$data']";
}
