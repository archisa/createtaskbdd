package com.avenuecode.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

public class LoginPage {
	
	public LoginPage(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
    
	@FindBy(how = How.XPATH, using = "//*[@id='user_email']")
	public WebElement username;
	
	@FindBy(how = How.XPATH, using = "//*[@id='user_password']")
	public WebElement password;
	
	@FindBy(how = How.XPATH, using = "//*[@name='commit']")
	public WebElement signIn;
	
	public void Login(String uname,String pass) {
		username.sendKeys(uname);
		password.sendKeys(pass);
		signIn.click();
	}
	
}
