Feature: As a ToDo App user
I should be able to create a subtask
So I can break down my tasks in smaller pieces

@selenium
Scenario: User should be able to see Manage Subtasks button for a task
Given User logs in
When User is logged in using username as "sood.archisa@yahoo.co.in" and password as "12345678"
And User enters value as "new task" in task field and presses enter
Then A new task should be created 
And User should be able to see Manage Subtasks button
Then I close the browser

@selenium
Scenario: User should not be able to edit TaskID and Task Description for Subtask
Given User logs in
When User is logged in using username as "sood.archisa@yahoo.co.in" and password as "12345678"
And User enters value as "new task" in task field and presses enter
Then A new task should be created 
And User clicks on Manage Subtasks button
Then TaskID and Description should be read only fields on Manage Subtasks modal dialog
Then I close the browser

@selenium
Scenario: Subtask description should not accept more than 250 characters
Given User logs in
When User is logged in using username as "sood.archisa@yahoo.co.in" and password as "12345678"
And User enters value as "new task" in task field and presses enter
Then A new task should be created 
And User clicks on Manage Subtasks button
Then SubTask Description should not accept more than 250 characters
Then I close the browser

@selenium
Scenario: User should be able to add a new subtask
Given User logs in
When User is logged in using username as "sood.archisa@yahoo.co.in" and password as "12345678"
And User enters value as "new task" in task field and presses enter
Then A new task should be created 
And User clicks on Manage Subtasks button 
And User enters value as "New Sub Task" and date as "15/11/2017"
And A new sub task should be created
And Number of subtasks are displayed on button
Then I close the browser

@selenium
Scenario: User should not be able to add a new subtask without date
Given User logs in
When User is logged in using username as "sood.archisa@yahoo.co.in" and password as "12345678"
And User enters value as "new task" in task field and presses enter
Then A new task should be created 
And User clicks on Manage Subtasks button 
And User enters value as "New Sub Task Without Due Date" 
And A new sub task should not be created
Then I close the browser

@selenium
Scenario: User should not be able to add a new subtask without subtask description
Given User logs in
When User is logged in using username as "sood.archisa@yahoo.co.in" and password as "12345678"
And User enters value as "new task" in task field and presses enter
Then A new task should be created 
And User clicks on Manage Subtasks button 
And User enters date as "16/11/2017" 
And A new sub task should not be created
Then I close the browser

@selenium
Scenario: User should be able to edit a subtask and save the changes
Given User logs in
When User is logged in using username as "sood.archisa@yahoo.co.in" and password as "12345678"
And User enters value as "new task" in task field and presses enter
Then A new task should be created 
And User clicks on Manage Subtasks button 
And User enters value as "New Sub Task" and date as "15/11/2017" 
And A new sub task should be created 
And User is able to edit the subtask desc with value as "Edit Task Desc"
And User is able to save changes with value as "Edit Task Desc" when he clicks on tick button
Then I close the browser

@selenium
Scenario: User should be able to edit a subtask and cancel the changes
Given User logs in
When User is logged in using username as "sood.archisa@yahoo.co.in" and password as "12345678"
And User enters value as "new task" in task field and presses enter
Then A new task should be created 
And User clicks on Manage Subtasks button 
And User enters value as "New Sub Task" and date as "15/11/2017" 
And A new sub task should be created 
And User is able to edit the subtask desc with value as "Edit Task Desc"
And User is able to cancel changes when he clicks on cancel button and value "Edit Task Desc" is not saved
Then I close the browser
