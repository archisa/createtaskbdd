Feature: As a ToDo App user
I should be able to create a task So I can manage my tasks

@selenium
Scenario: Correct Message is Displayed
Given User logs in
When User is logged in using username as "sood.archisa@yahoo.co.in" and password as "12345678"
Then The message "Hey Archisa Sood, this is your todo list for today:" should be displayed
Then I close the browser

@selenium
Scenario: User should be able to add new task
Given User logs in
When User is logged in using username as "sood.archisa@yahoo.co.in" and password as "12345678"
And User enters value as "new task" in task field and presses enter
Then A new task should be created
Then I close the browser

@selenium
Scenario: User should be able to add new task with button
Given User logs in
When User is logged in using username as "sood.archisa@yahoo.co.in" and password as "12345678"
And User clicks on Add Task button
Then Edit Empty Field To add description as "New Task Button"
Then A new task should be created
Then I close the browser

@selenium
Scenario: User should not be able to add less than 3 characters in task description
Given User logs in
When User is logged in using username as "sood.archisa@yahoo.co.in" and password as "12345678"
And User enters value as "ne" two characters in task field and presses enter
Then A new task should not be created
Then I close the browser

@selenium
Scenario: User should not be able to add more than 250 characters in task description
Given User logs in
When User is logged in using username as "sood.archisa@yahoo.co.in" and password as "12345678"
Then Verify max length for text field
Then I close the browser

@selenium
Scenario: User should not be able to edit task description with fewer than 2 chars when he clicks on tick button and cross button
Given User logs in
When User is logged in using username as "sood.archisa@yahoo.co.in" and password as "12345678"
And User clicks on Add Task button
Then Edit Empty Field To add description as "Ta"
And clicks on Tick Button
And clicks on Cross Button
Then A new task should not be created
Then I close the browser

@selenium
Scenario: User should not be able to edit task description with fewer than 3 characters
Given User logs in
When User is logged in using username as "sood.archisa@yahoo.co.in" and password as "12345678"
And User enters value as "new task" in task field and presses enter
Then A new task should be created
And User edits the newly created task with "Ed"
And clicks on Tick Button
Then Task description value should be unchanged "new task"
Then I close the browser

@selenium
Scenario: User should not be able to save changes when he clicks on cross button
Given User logs in
When User is logged in using username as "sood.archisa@yahoo.co.in" and password as "12345678"
And User enters value as "new task" in task field and presses enter
Then A new task should be created
And User edits the newly created task with "Edit"
And clicks on Cross Button
Then Task description value should be unchanged "new task"
Then I close the browser
